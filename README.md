*Copyright (C) 2022, Arthur SATOUF, Paris, France. All Rights Reserved.*
# Smarter-Mobility-Data-Challenge
**Author**\
Arthur Satouf

## Abstract
The key for this challenge was to ask yourself as many questions as you can imagine. It’s a bit tricky as a competition, as there are 3 hierarchical levels (“Stations”, “Area”, “Global”) and I found that it is not about finding the best model, it is more about making the best strategy to prepare your data to be modeled with a model that is recommended for multi long future steps. After trying and testing several techniques it turns out it would be best to consider the 3 levels of data sets independently of each other. We would treat them differently especially for filling the missing data and we should apply a different model for each set. A few of my inspiration questions were: How the data sets were exactly built for “Area” and “global”, where and what missing data do we need to recover and how to provide an output simple enough for this stochastic phenomena without any correction for the output manually. In addition, it was very important to connect the targets “Available”, “Charging”, “passive” and “other”. I have used CatBoost algorithms for the 3 levels. I have filled in a few missing data for “Stations” with EMW with 8 backward and 8 forwardand I have filled in all the missing data for “global” and “area”.


## Getting started
Below is the structure and scripts used in the challange:

```bash
├── Arthur SATOUF.pdf
├── README.md
├── data_to_use
│   ├── data_ewm1.2.csv
│   ├── remCharEWM4.csv
│   ├── test.csv
│   ├── train.csv
│   └── train_onlyNext4EWM4andBack_EWM_remChar.csv
├── main.py
├── notebook
│   ├── area_catboost.ipynb
│   ├── cleaning.ipynb
│   ├── global_catboost.ipynb
│   ├── station_catboost.ipynb
│   └── visualization.ipynb
├── sample_result_submission
│   ├── area.csv
│   ├── global.csv
│   └── station.csv
└── sample_result_submission.zip
```
* **Arthur SATOUF.pdf** project report.
* **data_to_use** - data used to build the models and to forecast
    * **train_onlyNext4EWM4andBack_EWM_remChar.csv** data set input for Station.
    * **data_ewm1.2.csv** data set input for Area.
    * **remCharEWM4.csv** data set inout for Global.
    * **train.csv** (The basic) it been used as a source to create the above data sets and  to get validation index to imporove model for Station by using it as valaidation set.
    * **test.csv** to be forecasted and submitted.
* **main.py** to model and build sample_result_submission using CatBoost. 
* **notebook** a bunch of note-bookS to build each model and ***cleaning.ipynb*** is used to clean and prepoessing the data to build the input data sets **data_to_use**
* **sample_result_submission** my submission. 
* **sample_result_submission.zip** submission en ZIP.

# How to run the code 
- Clone the project.
- Open Terminal.
- Insert ```python main.py``` and wait until it finish to gererat data please note that it would take (15 to 20 min).

## License

**[Apache License 2.0](../LICENSE)**
